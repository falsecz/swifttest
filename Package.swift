// swift-tools-version:5.3


import PackageDescription
let package = Package(
      name: "InstaCoverFramework",
      platforms: [
          .iOS(.v11)
      ],
      products: [
          .library(
              name: "InstaCoverFramework",
              targets: [
                "RCTVibration","folly","react_native_webview","FBReactNativeSpec","RCTLinking","glog","jsi","cxxreact","RCTBlob","Pods_InstaCoverFramework","RCTAnimation","CoreModules","InstaCoverFramework","RCTTypeSafety","RCTSettings","yoga","React","RCTNetwork","ReactCommon","jsinspector","DoubleConversion","jsireact","RCTImage","fmt","RCTText","reactperflogger",

              ])
      ],
      targets: [
        .binaryTarget(name: "RCTVibration", path: "RCTVibration.xcframework"),
.binaryTarget(name: "folly", path: "folly.xcframework"),
.binaryTarget(name: "react_native_webview", path: "react_native_webview.xcframework"),
.binaryTarget(name: "FBReactNativeSpec", path: "FBReactNativeSpec.xcframework"),
.binaryTarget(name: "RCTLinking", path: "RCTLinking.xcframework"),
.binaryTarget(name: "glog", path: "glog.xcframework"),
.binaryTarget(name: "jsi", path: "jsi.xcframework"),
.binaryTarget(name: "cxxreact", path: "cxxreact.xcframework"),
.binaryTarget(name: "RCTBlob", path: "RCTBlob.xcframework"),
.binaryTarget(name: "Pods_InstaCoverFramework", path: "Pods_InstaCoverFramework.xcframework"),
.binaryTarget(name: "RCTAnimation", path: "RCTAnimation.xcframework"),
.binaryTarget(name: "CoreModules", path: "CoreModules.xcframework"),
.binaryTarget(name: "InstaCoverFramework", path: "InstaCoverFramework.xcframework"),
.binaryTarget(name: "RCTTypeSafety", path: "RCTTypeSafety.xcframework"),
.binaryTarget(name: "RCTSettings", path: "RCTSettings.xcframework"),
.binaryTarget(name: "yoga", path: "yoga.xcframework"),
.binaryTarget(name: "React", path: "React.xcframework"),
.binaryTarget(name: "RCTNetwork", path: "RCTNetwork.xcframework"),
.binaryTarget(name: "ReactCommon", path: "ReactCommon.xcframework"),
.binaryTarget(name: "jsinspector", path: "jsinspector.xcframework"),
.binaryTarget(name: "DoubleConversion", path: "DoubleConversion.xcframework"),
.binaryTarget(name: "jsireact", path: "jsireact.xcframework"),
.binaryTarget(name: "RCTImage", path: "RCTImage.xcframework"),
.binaryTarget(name: "fmt", path: "fmt.xcframework"),
.binaryTarget(name: "RCTText", path: "RCTText.xcframework"),
.binaryTarget(name: "reactperflogger", path: "reactperflogger.xcframework"),

      ])
